import Vue from 'vue' 
import App from './App.vue'
import Home from './components/Home.vue'
import Pager from './page/Pager.vue'


Vue.component("Lamp-component", Home);
Vue.component("pageType" , Pager)

new Vue({
  el: '#app',
  render: h => h(App)
})
